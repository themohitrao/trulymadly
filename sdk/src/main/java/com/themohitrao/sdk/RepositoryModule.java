package com.themohitrao.sdk;

import com.themohitrao.common.models.GitRepoModel;

import java.util.List;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Single;
import retrofit2.Retrofit;

import static com.themohitrao.sdk.DataModule.LANGUAGE;
import static com.themohitrao.sdk.DataModule.SINCE_WHEN;

@Module(includes = {ApiClientModule.class, DataModule.class})
class RepositoryModule {

    @Provides
    Single<List<GitRepoModel>> getRepoList(Retrofit retrofit,
                                           Class<ApiInterface> service,
                                           @Named(LANGUAGE) String language,
                                           @Named(SINCE_WHEN) String sinceWhen) {
        return retrofit.create(service).getGitrepoList(language, sinceWhen);
    }

    @NetworkScope
    @Provides
    Class<ApiInterface> getService() {
        return ApiInterface.class;
    }
}
