package com.themohitrao.sdk;

import com.themohitrao.common.models.GitRepoModel;

import java.util.List;

import javax.inject.Inject;

import dagger.Component;
import io.reactivex.Single;

@NetworkScope

@Component(modules = RepositoryModule.class)
public interface RepositoryComponent {

    Single<List<GitRepoModel>> getRepoList();

}
