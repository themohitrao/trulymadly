package com.themohitrao.sdk;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class DataModule {

    public static final String LANGUAGE = "languageQualifier";
    public static final String SINCE_WHEN = "sinceWhenQualifier";

    private String mLanguage;
    private String mSinceWhen;

    public DataModule(String mLanguage, String mSinceWhen) {
        this.mLanguage = mLanguage;
        this.mSinceWhen = mSinceWhen;
    }

    @Provides
    @Named(LANGUAGE)
    String getLanguage(){
        return mLanguage;
    }

    @Provides
    @Named(SINCE_WHEN)
    String getSinceWhen(){
        return mSinceWhen;
    }
}
