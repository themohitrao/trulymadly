package com.themohitrao.sdk;

import com.themohitrao.common.models.GitRepoModel;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Public contract to define GET API Call Details.
 * to be used in creating the connection call.
 */
interface ApiInterface {

    @GET("developers")
    Single<List<GitRepoModel>> getGitrepoList(@Query("language") String language,
                                              @Query("since") String sinceWhen);
}
