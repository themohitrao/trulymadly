package com.themohitrao.core;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;

import com.themohitrao.common.models.GitRepoModel;
import com.themohitrao.sdk.DaggerRepositoryComponent;
import com.themohitrao.sdk.DataModule;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class GitRepoListViewModel extends AndroidViewModel {

    private MutableLiveData<List<GitRepoModel>> mGitRepoList;
    private MutableLiveData<Boolean> mLoaderVisibility;
    private MutableLiveData<Boolean> mSubTitleVisibility;
    private MutableLiveData<String> mErrorMessage;
    private CompositeDisposable mCompositeDisposable;
    private MutableLiveData<String> mSubTitleMessage;

    GitRepoListViewModel(Application application) {
        super(application);
        initializeComponents();
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (mCompositeDisposable != null) {
            mCompositeDisposable.dispose();
            mCompositeDisposable = null;
        }
    }

    private void initializeComponents() {
        mGitRepoList = new MutableLiveData<>();
        mLoaderVisibility = new MutableLiveData<>();
        mSubTitleVisibility = new MutableLiveData<>();
        mErrorMessage = new MutableLiveData<>();
        mCompositeDisposable = new CompositeDisposable();
        mSubTitleMessage = new MutableLiveData<>();
        mSubTitleVisibility.setValue(true);
    }

    void onSuccess(List<GitRepoModel> gitRepoModels) {
        mLoaderVisibility.setValue(false);
        if (gitRepoModels == null || gitRepoModels.isEmpty()) {
            mErrorMessage.setValue("list is empty");
            mGitRepoList.setValue(new ArrayList<>());
        } else {
            mGitRepoList.setValue(gitRepoModels);
        }
    }

    void onError(Throwable throwable) {
        mErrorMessage.setValue(throwable.getLocalizedMessage());
        mLoaderVisibility.setValue(false);
    }

    public MutableLiveData<List<GitRepoModel>> getGitRepoList() {
        return mGitRepoList;
    }

    /**
     * Method to initiate request for fetching data.
     *
     * @param language  language which needs to be searched
     * @param sinceWhen time frame: weekly,montly,=yearly
     */
    public void initiateRequest(String language, String sinceWhen) {
        mLoaderVisibility.setValue(true);
        mCompositeDisposable.add(getDisposable(language, sinceWhen));
    }

    /**
     * Method to return Disposable instance for an rx call.
     *
     * @param language  language which needs to be searched
     * @param sinceWhen time frame: weekly,montly,=yearly
     * @return instance of Disposable
     */
    private Disposable getDisposable(String language, String sinceWhen) {
        return DaggerRepositoryComponent.builder()
                .dataModule(new DataModule(language, sinceWhen))
                .build()
                .getRepoList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccess, this::onError);
    }


    public MutableLiveData<String> getErrorMessage() {
        return mErrorMessage;
    }

    public MutableLiveData<Boolean> getLoaderVisibility() {
        return mLoaderVisibility;
    }

    public MutableLiveData<String> getSubTitleMessage() {
        return mSubTitleMessage;
    }

    public void setSubTitleMessage(String message) {
        this.mSubTitleMessage.setValue(message);
    }

    public MutableLiveData<Boolean> getSubTitleVisibility() {
        return mSubTitleVisibility;
    }

    public void setSubTitleVisibility(boolean subTitleVisibility) {
        this.mSubTitleVisibility.setValue(subTitleVisibility);
    }
}