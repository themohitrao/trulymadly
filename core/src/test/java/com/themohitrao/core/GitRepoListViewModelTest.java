package com.themohitrao.core;

import android.app.Application;
import android.arch.core.executor.testing.InstantTaskExecutorRule;

import com.themohitrao.common.models.GitRepoModel;

import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class GitRepoListViewModelTest {

    private static final String DUMMY_STRING = "dummyString";
    @ClassRule
    public static final InstantTaskExecutorRule rule = new InstantTaskExecutorRule();
    @ClassRule
    public static final RxImmediateSchedulerRule newRule = new RxImmediateSchedulerRule();

    GitRepoListViewModel mGitRepoListViewModel;

    @Before
    public void setUp() {
        Application application = Mockito.mock(Application.class);
        mGitRepoListViewModel = new GitRepoListViewModel(application);
        setUpSchedulers();
    }

    private void setUpSchedulers() {
    }

    private List<GitRepoModel> getDummyGitRepoList(boolean isEmpty) {
        if(isEmpty) {
            return new ArrayList<>();
        }else{
            List<GitRepoModel> list = new ArrayList<>();
            list.add(new GitRepoModel());
            return list;
        }
    }

    @Test
    public void dataNotNullOnInitialization() {
        assertNotNull(mGitRepoListViewModel.getErrorMessage());
        assertNotNull(mGitRepoListViewModel.getSubTitleMessage());
        assertNotNull(mGitRepoListViewModel.getSubTitleVisibility());
        assertNotNull(mGitRepoListViewModel.getGitRepoList());
        assertNotNull(mGitRepoListViewModel.getLoaderVisibility());
    }

    @Test
    public void getLoadingVisibilityOnRequestInitiation() {
        mGitRepoListViewModel.initiateRequest(DUMMY_STRING, DUMMY_STRING);
        assertTrue(mGitRepoListViewModel.getLoaderVisibility().getValue());
    }

    @Test
    public void getLoadingVisibilityOnSuccessFullResponseButEmptyList() {
        mGitRepoListViewModel.onSuccess(getDummyGitRepoList(true));
        assertFalse(mGitRepoListViewModel.getLoaderVisibility().getValue());
    }

    @Test
    public void getLoadingVisibilityOnSuccessFullResponseButNonEmptyList() {
        mGitRepoListViewModel.onSuccess(getDummyGitRepoList(false));
        assertFalse(mGitRepoListViewModel.getLoaderVisibility().getValue());
    }

    @Test
    public void getLoadingVisibilityOnError() {
        mGitRepoListViewModel.onError(new Throwable());
        assertFalse(mGitRepoListViewModel.getLoaderVisibility().getValue());
    }

}