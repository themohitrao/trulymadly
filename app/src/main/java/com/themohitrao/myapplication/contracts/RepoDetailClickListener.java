package com.themohitrao.myapplication.contracts;

import com.themohitrao.myapplication.fragments.GitRepoDetailFragment;

/**
 * Contract for defining click listeners of {@link GitRepoDetailFragment}
 */
public interface RepoDetailClickListener {
    /**
     * Callback for click event of username on detail page
     */
    void onClickUserName();

    /**
     * Callback for click event of visit Repo Button on detail page.
     */
    void onClickVisitRepo();
}
