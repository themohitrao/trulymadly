package com.themohitrao.myapplication.contracts;

import com.themohitrao.common.models.GitRepoModel;

public interface GitRepoSelectionListener {

    void onGitRepoSelected(GitRepoModel selectedObjected);
}
