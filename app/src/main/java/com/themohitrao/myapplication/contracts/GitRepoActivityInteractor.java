package com.themohitrao.myapplication.contracts;

import com.themohitrao.core.GitRepoListViewModel;

public interface GitRepoActivityInteractor {

    GitRepoListViewModel getViewModel();

    void setSubTitleMessage(String messageHeader);

}
