package com.themohitrao.myapplication.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.themohitrao.common.models.GitRepoModel;
import com.themohitrao.myapplication.R;
import com.themohitrao.myapplication.adapters.GitRepoListAdapter;
import com.themohitrao.myapplication.contracts.GitRepoActivityInteractor;
import com.themohitrao.myapplication.contracts.GitRepoSelectionListener;
import com.themohitrao.myapplication.databinding.FragmentGitRepoListBinding;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link GitRepoListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GitRepoListFragment extends Fragment {

    private GitRepoSelectionListener mListener;
    private GitRepoListAdapter mGitRepoListAdapter;
    private GitRepoActivityInteractor mGitRepoActivityInteractor;

    public GitRepoListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment GitRepoListFragment.
     */
    public static GitRepoListFragment newInstance() {
        return new GitRepoListFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof GitRepoSelectionListener) {
            mListener = (GitRepoSelectionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement GitRepoSelectionListener");
        }

        if (context instanceof GitRepoActivityInteractor) {
            mGitRepoActivityInteractor = (GitRepoActivityInteractor) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement GitRepoActivityInteractor");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentGitRepoListBinding binding = FragmentGitRepoListBinding.inflate(inflater);
        binding.setGitRepoListViewModel(mGitRepoActivityInteractor.getViewModel());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addObservers();
        setupList(view);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        mGitRepoActivityInteractor = null;
    }


    private void addObservers() {
        mGitRepoActivityInteractor.getViewModel().getErrorMessage().observe(this, this::onError);
        mGitRepoActivityInteractor.getViewModel().getGitRepoList().observe(this, this::onNewListFetched);
    }

    private void onNewListFetched(List<GitRepoModel> gitRepoModels) {
        if(gitRepoModels.isEmpty()){
            mGitRepoActivityInteractor.setSubTitleMessage(getResources().getString(R.string.no_results_for));
        }else{
            mGitRepoActivityInteractor.setSubTitleMessage(getResources().getString(R.string.showing_results_for));
        }
        mGitRepoListAdapter.setGitRepoList(gitRepoModels);
        mGitRepoListAdapter.notifyDataSetChanged();
    }

    private void onError(String errorMessage) {
        Toast.makeText(getContext(), "" + errorMessage, Toast.LENGTH_SHORT).show();
    }

    /**
     * function to setup the git repo list
     *
     * @param view fragments view
     */
    private void setupList(View view) {
        RecyclerView recyclerViewRepoList = view.findViewById(R.id.recyclerview_list);
        recyclerViewRepoList.setLayoutManager(new LinearLayoutManager(getContext()));
        mGitRepoListAdapter = new GitRepoListAdapter(mListener);
        recyclerViewRepoList.addItemDecoration(
                new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        recyclerViewRepoList.setAdapter(mGitRepoListAdapter);
    }
}
