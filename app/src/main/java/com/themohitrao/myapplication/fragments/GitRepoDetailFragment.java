package com.themohitrao.myapplication.fragments;


import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.themohitrao.common.models.GitRepoModel;
import com.themohitrao.myapplication.contracts.RepoDetailClickListener;
import com.themohitrao.myapplication.databinding.FragmentGitRepoDetailBinding;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link GitRepoDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GitRepoDetailFragment extends Fragment implements RepoDetailClickListener {

    private GitRepoModel mGitRepoModel;


    public GitRepoDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param gitRepoModel git repository object.
     * @return A new instance of fragment GitRepoDetailFragment.
     */
    public static GitRepoDetailFragment newInstance(GitRepoModel gitRepoModel) {
        GitRepoDetailFragment fragment = new GitRepoDetailFragment();
        fragment.mGitRepoModel = gitRepoModel;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentGitRepoDetailBinding binding = FragmentGitRepoDetailBinding.inflate(inflater);
        binding.setMGitRepoModel(mGitRepoModel);
        binding.setClickListener(this);
        return binding.getRoot();
    }

    @Override
    public void onClickUserName() {
        openLink(mGitRepoModel.getUrl());
    }

    @Override
    public void onClickVisitRepo() {
        openLink(mGitRepoModel.getRepoDetails().getUrl());
    }


    private void openLink(String url) {
        if (url == null || url.equals("")) {
            Toast.makeText(getContext(), "No valid url found", Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(myIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(getContext(), "No application can handle this request."
                    + " Please install a webbrowser", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

}
