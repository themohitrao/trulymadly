package com.themohitrao.myapplication.adapters;

import android.databinding.BindingAdapter;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.themohitrao.common.lrucache.LRUCacheFactory;
import com.themohitrao.common.models.GitRepoModel;
import com.themohitrao.common.util.AppCoreConstants;
import com.themohitrao.myapplication.R;

import java.util.List;

/**
 * Helper Class for defining various binding adapter methods
 */
public class HelperBindingAdapter {

    private HelperBindingAdapter() {
        //METHOD NOT IN USE
    }

    /**
     * This method provides the functionality to set a list into a recyclerview
     *
     * @param rclView recyclerview which needs to be set list in
     * @param data    list which is to be set
     */
    @BindingAdapter("list")
    public static void setList(RecyclerView rclView, List<GitRepoModel> data) {
        if (data != null) {
            GitRepoListAdapter gitRepoListAdapter = ((GitRepoListAdapter) rclView.getAdapter());
            gitRepoListAdapter.setGitRepoList(data);
            rclView.getLayoutManager().scrollToPosition(AppCoreConstants.ZERO);
            gitRepoListAdapter.notifyDataSetChanged();
        }
    }

    /**
     * Method to set image into an image view
     *
     * @param imgView target imageview
     * @param imgUrl  url of the imageview bitmap
     */
    @BindingAdapter("imgUrl")
    public static void setImage(ImageView imgView, String imgUrl) {

        Bitmap bitmap = LRUCacheFactory.getInstance().getBitmap(imgUrl);

        if (bitmap == null) {
            Glide.with(imgView.getContext())
                    .load(imgUrl)
                    .asBitmap()
                    .placeholder(R.drawable.layout_shape_round)
                    .error(R.drawable.layout_shape_round)
                    .into(new BitmapImageViewTarget(imgView) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            LRUCacheFactory.getInstance().insertBitmap(imgUrl, resource);
                            imgView.setImageBitmap(resource);
                        }
                    });
        } else {
            imgView.setImageBitmap(bitmap);
        }
    }

    /**
     * Method to set round image into an image view
     *
     * @param imageView target imageview
     * @param imgUrl    url of the imageview bitmap
     */
    @BindingAdapter("imgRoundUrl")
    public static void setRoundImage(ImageView imageView, String imgUrl) {
        Bitmap bitmap = LRUCacheFactory.getInstance().getBitmap(imgUrl);

        if (bitmap == null) {
            Glide.with(imageView.getContext())
                    .load(imgUrl)
                    .asBitmap()
                    .centerCrop()
                    .dontAnimate()
                    .placeholder(R.drawable.layout_shape_round)
                    .error(R.drawable.layout_shape_round)
                    .into(new BitmapImageViewTarget(imageView) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            LRUCacheFactory.getInstance().insertBitmap(imgUrl, resource);
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(imageView.getContext().getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            imageView.setImageDrawable(circularBitmapDrawable);
                        }
                    });
        } else {
            RoundedBitmapDrawable circularBitmapDrawable =
                    RoundedBitmapDrawableFactory.create(imageView.getContext().getResources(), bitmap);
            circularBitmapDrawable.setCircular(true);
            imageView.setImageDrawable(circularBitmapDrawable);
        }
    }

    /**
     * Method to set underlined text
     *
     * @param textView target text view
     * @param text     string value to be underlined and set.
     */
    @BindingAdapter("setUnderLinedText")
    public static void setUnderline(TextView textView, String text) {
        if (text == null) {
            text = "";
        }
        SpannableString content = new SpannableString(text);
        content.setSpan(new UnderlineSpan(), 0, text.length(), 0);
        textView.setText(content);
    }

}
