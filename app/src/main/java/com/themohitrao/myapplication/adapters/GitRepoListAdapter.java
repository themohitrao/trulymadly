/*
 * Copyright (c) 2019 McDonald's. All rights reserved.
 * Created by devsharm6 on 01-Apr-2019.
 */

package com.themohitrao.myapplication.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.themohitrao.myapplication.databinding.LayoutGitRepoItemBinding;
import com.themohitrao.common.models.GitRepoModel;
import com.themohitrao.myapplication.contracts.GitRepoSelectionListener;
import com.themohitrao.myapplication.viewholders.GitRepoItemViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter for history list recycler view
 */
public class GitRepoListAdapter extends RecyclerView.Adapter<GitRepoItemViewHolder> {

    private List<GitRepoModel> mGitRepoList;
    private GitRepoSelectionListener mClickListener;

    public GitRepoListAdapter(GitRepoSelectionListener clickListener) {
        this.mGitRepoList = new ArrayList<>();
        this.mClickListener = clickListener;
    }

    @Override
    public GitRepoItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GitRepoItemViewHolder(
                LayoutGitRepoItemBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull GitRepoItemViewHolder gitRepoItemViewHolder, int position) {
        if (!mGitRepoList.isEmpty()) {
            gitRepoItemViewHolder.bind(mGitRepoList.get(position));
            gitRepoItemViewHolder.getBinding().setClickListener(mClickListener);
        }
    }

    @Override
    public void onViewDetachedFromWindow(GitRepoItemViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return mGitRepoList.size();
    }

    public void setGitRepoList(List<GitRepoModel> newList) {
        this.mGitRepoList.clear();
        this.mGitRepoList.addAll(newList);
    }
}
