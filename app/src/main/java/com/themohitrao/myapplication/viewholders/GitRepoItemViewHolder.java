package com.themohitrao.myapplication.viewholders;

import android.support.v7.widget.RecyclerView;

import com.themohitrao.myapplication.databinding.LayoutGitRepoItemBinding;
import com.themohitrao.common.models.GitRepoModel;

/**
 * View Holder class for the object of {@link GitRepoModel}
 */
public class GitRepoItemViewHolder extends RecyclerView.ViewHolder {
    private final LayoutGitRepoItemBinding mBinding;

    public GitRepoItemViewHolder(LayoutGitRepoItemBinding mBinding) {
        super(mBinding.getRoot());
        this.mBinding = mBinding;
    }

    /**
     * Method to bind view
     *
     * @param item model object
     */
    public void bind(GitRepoModel item) {
        mBinding.setGitRepoModel(item);
        mBinding.executePendingBindings();
    }

    /**
     * Method to unbind view
     */
    public void unbind() {
        if (mBinding != null) {
            mBinding.unbind();
        }
    }

    public LayoutGitRepoItemBinding getBinding() {
        return mBinding;
    }
}