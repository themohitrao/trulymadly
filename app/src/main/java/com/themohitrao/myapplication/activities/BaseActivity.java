package com.themohitrao.myapplication.activities;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.themohitrao.myapplication.R;

public class BaseActivity extends AppCompatActivity {


    /**
     * Method to hide the status bar to make it a full Screen Activity
     */
    protected void hideStatusBar() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
    @Override
    public void onBackPressed() {
        handleFragmentBackStack();
    }

    /**
     * Method to launch fragments for this activity
     *
     * @param fragment object of fragment intended to be launched
     */
    protected void launchFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(
                findViewById(R.id.frame_layout).getId(),
                fragment);
        transaction.addToBackStack(fragment.getClass().getSimpleName());
        transaction.commit();
        getSupportFragmentManager().executePendingTransactions();
    }

    /**
     * Method to handle Fragments on back press button
     */
    protected void handleFragmentBackStack() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
        } else {
            super.onBackPressed();
        }
    }

}
