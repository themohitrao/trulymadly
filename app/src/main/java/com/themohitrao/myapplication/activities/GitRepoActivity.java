package com.themohitrao.myapplication.activities;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.themohitrao.common.models.GitRepoModel;
import com.themohitrao.myapplication.R;
import com.themohitrao.myapplication.databinding.ActivityGitRepoBinding;
import com.themohitrao.core.GitRepoListViewModel;
import com.themohitrao.core.ViewModelFactory;
import com.themohitrao.myapplication.contracts.GitRepoActivityInteractor;
import com.themohitrao.myapplication.contracts.GitRepoSelectionListener;
import com.themohitrao.myapplication.fragments.GitRepoDetailFragment;
import com.themohitrao.myapplication.fragments.GitRepoListFragment;

import static android.support.v4.view.MenuItemCompat.getActionView;

/**
 * Main Class to populate and navigate details for the top repository list of git
 */
public class GitRepoActivity extends BaseActivity implements GitRepoSelectionListener,
        GitRepoActivityInteractor {

    private String mSelectedLanguage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideStatusBar();
        setUpBinding();
        setUpToolbar();
        launchFragment(GitRepoListFragment.newInstance());
        initiateRequest("java", "weekly");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        MenuItem searchViewItem = menu.findItem(R.id.action_search);
        setupSearch(searchViewItem);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onGitRepoSelected(GitRepoModel selectedObjected) {
        getRepoListViewModel().setSubTitleVisibility(false);
        launchFragment(GitRepoDetailFragment.newInstance(selectedObjected));

    }

    @Override
    public GitRepoListViewModel getViewModel() {
        return getRepoListViewModel();
    }

    @Override
    public void setSubTitleMessage(String messageHeader) {
        getRepoListViewModel().setSubTitleMessage(String.format(messageHeader, mSelectedLanguage));
    }

    @Override
    public void onBackPressed() {
        getRepoListViewModel().setSubTitleVisibility(true);
        super.onBackPressed();
    }

    /**
     * Method to setup SearchView
     *
     * @param searchViewItem search view instance
     */
    private void setupSearch(MenuItem searchViewItem) {
        final SearchView searchView = (SearchView) getActionView(searchViewItem);
        searchView.setQueryHint(getResources().getString(R.string.text_search_hint));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.clearFocus();
                mSelectedLanguage = query;
                initiateRequest(query, "weekly");
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    /**
     * Mehtod to setup toolbar
     */
    private void setUpToolbar() {
        setSupportActionBar(findViewById(R.id.toolbar));
        getSupportActionBar().setIcon(R.drawable.app_logo);
    }

    /**
     * method to request data from repository via view model
     *
     * @param language  string value of language which needs to be fetched data against
     * @param sinceWhen string value : weekly, monthly, yearly
     */
    private void initiateRequest(String language, String sinceWhen) {
        getRepoListViewModel().setSubTitleMessage(getString(com.themohitrao.core.R.string.loading_results_for, language));
        getRepoListViewModel().initiateRequest(language, sinceWhen);
    }

    /**
     * Mehod to set the Data Biding and xml initialization
     */
    private void setUpBinding() {
        ActivityGitRepoBinding binding
                = DataBindingUtil.setContentView(this, R.layout.activity_git_repo);
        binding.setLifecycleOwner(this);
        binding.setViewModel(getRepoListViewModel());
    }

    public GitRepoListViewModel getRepoListViewModel() {
        return ViewModelProviders.of(this,
                ViewModelFactory.getInstance(getApplication()))
                .get(GitRepoListViewModel.class);
    }
}
